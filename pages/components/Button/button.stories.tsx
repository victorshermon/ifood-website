import React from 'react'

import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Button } from './Button'

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    children: {
      type: 'string'
    }
  }
} as ComponentMeta<typeof Button>

export const ButtonTest: ComponentStory<typeof Button> = (args) => (
  <Button {...args} />
)

ButtonTest.args = {
  children: 'Login'
}
